const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
    const token = req.header('Authorization')
    if (!token)
        return res
            .status(401)
            .json({ 'message': 'Unauthenticated' })

    try {
        const decoded = jwt.verify(token, "auth_admin");
        req.user = decoded.user;
        next();
    } catch (e) {
        console.log(e);
        res.status(500).send({ message: "Invalid Token" });
    }
}