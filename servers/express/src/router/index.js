const express = require('express')
const middleware = require('../middleware/auth')
const router = express.Router()
const userController = require('../controller/UserControleller')
router.use('/api/', [
    router.use('/user', [
        router.post('/login', userController.login),

        router.post('/create', middleware, userController.createUpdate),
        router.delete('/remove/:ID', middleware, userController.deleteUser),
        router.get('/me', middleware, userController.me),
        router.get('/all', middleware, userController.getAll)

    ])
])


module.exports = router