'use strict'

const mongoose = require('mongoose')
const app = require('./app')
require('dotenv').config({ 'path': '.env' })

mongoose.connect(process.env.DB_CONNECTION, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true
})


mongoose.Promise = global.Promise

const db = mongoose.connection

db.on('error', (err) => {
    console.log(`Field to connect with mongodb ===> ${err}`);
})

db.on('open', () => {
    console.log('connection successffully setup');
})

const PORT = process.env.APP_PORT || 3001

const server = app.listen(PORT, () => {
    console.log(`server runing on  http://localhost:${server.address().port}`);
})